# sca-db

Connect ot pg with support for `node-config` configuration.

It uses the wonderful `pg-promise`.

## Configuration

- `db.pgpConfig` {}

- `db.database`

- `db.user`

- `db.password`

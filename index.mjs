
import config from 'config';
import pgpFactory from 'pg-promise';

export default (configs) => {
  const defaults = {
    pgpConfig: {
      noLocking: true,
    },
  };

  // Mixin configs that have been passed in, and make those my defaults
  config.util.extendDeep(defaults, configs);
  config.util.setModuleDefaults('db', defaults);

  const db = pgpFactory(config.get('db.pgpConfig'))({
    database: config.get('db.database'),
    user: config.get('db.user'),
    password: config.get('db.password'),
  });

  const overrideMethods = [
    'none',
    'one',
    'oneOrNone',
    'many',
    'manyOrNone',
    'any',
    'query',
  ];

  for (const method of overrideMethods) {
    const methodFn = db[ method ];

    db[ method ] = (q, ...args) => {
      if (q && typeof q === 'object' && q.sql) {
        return methodFn(q.sql, q.parameters, ...args);
      } else {
        return methodFn(q, ...args);
      }
    };
  }

  return db;
};
